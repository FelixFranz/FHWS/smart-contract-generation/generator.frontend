import React from 'react';
import Router from './compontents/Router.js';
import axios from 'axios';

export default class App extends React.Component {

	componentWillMount(){
		this.useInterceptor();
	}

	useInterceptor(){
		let apiUrl;
		if ("localhost" === window.location.hostname)
			apiUrl = "https://generator.felix-franz.com/api";
		else
			apiUrl = "/api";
		axios.interceptors.request.use(request =>{
			if (request.url.startsWith("$API_URL"))
				request.url = request.url.replace("$API_URL", apiUrl)
			return request;
		});
	}

	render(){
		return <Router />;
	}
}
