import React from 'react';
import './footer.css';

export default class Footer extends React.Component {

    render() {
        return (
            <div className="footer">
                <p className="footer">&copy; 2018 &ndash; 2020 by <a className="footer" href="https://www.felix-franz.com">
                    Felix Franz</a> | All rights reserved</p>
            </div>
        );
    }
}