import React from 'react';
import { Redirect } from 'react-router-dom'

export default class Generator extends React.Component {

	render() {
		return <Redirect to={"/generator/session"} />;
	}
}
