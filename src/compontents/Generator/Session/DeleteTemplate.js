import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import axios from "axios";
import {Toast} from "../../Toast";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faTrash} from "@fortawesome/free-solid-svg-icons";

export default class DeleteTemplate extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			modalShown: false
		};
		this.onDeleteTemplate = this.onDeleteTemplate.bind(this);
		this.showModal = this.showModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	onDeleteTemplate(e){
		axios({
			method: "DELETE",
			url: "$API_URL/session/" + this.props.sessionId + "/template",
			data: {
				name: this.props.name
			}
		})
			.then(response =>{
				this.props.onTemplateDeleted(this.props.name);
				Toast.showSuccess("Removed template");
			})
			.catch(response => Toast.showError("Could not remove template"));
	}

	showModal(){
		this.setState({
			modalShown: true
		});
	}

	closeModal(){
		this.setState({
			modalShown: false
		});
	}

	render() {
		return (
			<div>
			<Button bsStyle="primary" onClick={this.showModal} className="pull-right">
				<FontAwesomeIcon icon={faTrash}/>
			</Button>
			<Modal show={this.state.modalShown} onHide={this.closeModal}>
				<Modal.Header closeButton>
					<Modal.Title>Delete template</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{"Do you really want to delete the template " + this.props.name + "?"}
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle="primary" onClick={this.closeModal}>Close</Button>
					<Button bsStyle="primary" onClick={this.onDeleteTemplate}>Delete template</Button>
				</Modal.Footer>
			</Modal>
			</div>
		);
	}
}