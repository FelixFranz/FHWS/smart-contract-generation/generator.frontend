import React from 'react';
import {ListGroup, ListGroupItem} from 'react-bootstrap';
import DeleteTemplate from "./DeleteTemplate";
import AddTemplate from "./AddTemplate";

export default class Templates extends React.Component {

    constructor(props) {
        super(props);
        this.onTemplateAdded = this.onTemplateAdded.bind(this);
        this.onTemplateDeleted = this.onTemplateDeleted.bind(this);
    }

    onTemplateAdded(template) {
        let templates = this.props.templates;
        templates.push(template);
        this.props.updateTemplates(templates);
    }

    onTemplateDeleted(name) {
        let templates = this.props.templates.filter(t => t.name !== name);
        this.props.updateTemplates(templates);
    }

    render() {
        let content;
        if (this.props.templates.length === 0)
            content = (
                <p>No template added, just add a new one!</p>
            );
        else {
            content = this.props.templates.map((t, i) => {
                return (
                    <ListGroupItem key={i}>
                        <DeleteTemplate sessionId={this.props.sessionId} name={t.name}
                                        onTemplateDeleted={this.onTemplateDeleted}/>
                        <p>
                            <strong>Name:</strong> <span className="templateName">{t.name}</span>
                            <br/>
                            <strong>Variant:</strong> <span>{t.variant}</span>
                            <br/>
                            <strong>Constructor Variables: </strong> <span>{t.constructorVariable.join(", ")}</span>
                        </p>
                    </ListGroupItem>
                )
            });
            content = <ListGroup>{content}</ListGroup>;
        }
        return (
            <div>
                <AddTemplate sessionId={this.props.sessionId} onTemplateAdded={this.onTemplateAdded}/>
                <h2>Templates</h2>
                {content}
            </div>
        );
    }
}