import React from 'react';
import "./Export.css"
import {Grid, Row, Col, OverlayTrigger, Tooltip, Button} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronCircleLeft} from "@fortawesome/free-solid-svg-icons";

export default class Export extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sessionId: this.props.match.params.sessionId
        }
    }

    render() {
        return (
            <div>
                <h1>Export</h1>
                <p>Select one of following export types</p>
                <h2>Export Binaries</h2>
                <Grid>
                    <Row className="show-grid">
                        <Col xs={12} sm={6}>
                            <OverlayTrigger placement="bottom" overlay={
                                <Tooltip id="ExportConfig">Build and deploy the Smart Contract to a Network using <a
                                    href="https://metamask.io/"
                                    target="_blank" rel="noopener noreferrer">MetaMask</a>.</Tooltip>
                            }>
                                <Button bsStyle="primary" className="btn-maxWidth"
                                        href={"/generator/session/" + this.state.sessionId + "/export/deploy"}
                                        rel="noopener noreferrer">
                                    Deploy Smart Contract
                                </Button>
                            </OverlayTrigger>
                        </Col>
                        <Col xs={12} sm={6}>
                            <OverlayTrigger placement="bottom" overlay={
                                <Tooltip id="ExportBinaries">Build and export a json file containing the compiled
                                    contract.</Tooltip>
                            }>
                                <Button bsStyle="primary" className="btn-maxWidth"
                                        href={"/generator/session/" + this.state.sessionId + "/export/binaries"}
                                        rel="noopener noreferrer">
                                    Build Binaries
                                </Button>
                            </OverlayTrigger>
                        </Col>
                    </Row>
                </Grid>
                <h2>Export Source</h2>
                <Grid>
                    <Row className="show-grid">
                        <Col xs={12} sm={4}>
                            <OverlayTrigger placement="bottom" overlay={
                                <Tooltip id="ExportConfig">Export a file with the current contract configuration that
                                    can be imported into the generator later.</Tooltip>
                            }>
                                <Button bsStyle="primary" className="btn-maxWidth"
                                        href={"/generator/session/" + this.state.sessionId + "/export/config"}
                                        rel="noopener noreferrer">
                                    Export Config
                                </Button>
                            </OverlayTrigger>
                        </Col>
                        <Col xs={12} sm={4}>
                            <OverlayTrigger placement="bottom" overlay={
                                <Tooltip id="ExportSource">Export a zip file with the contract source code, splitted in
                                    multiple files.</Tooltip>
                            }>
                                <Button bsStyle="primary" className="btn-maxWidth"
                                        href={"/generator/session/" + this.state.sessionId + "/export/source"}
                                        rel="noopener noreferrer">
                                    Export Source
                                </Button>
                            </OverlayTrigger>
                        </Col>
                        <Col xs={12} sm={4}>
                            <OverlayTrigger placement="bottom" overlay={
                                <Tooltip id="ExportOneFileSource">
                                    Build and export a file that contains the whole source. With this file you can verify your
                                    contract at <a href="https://etherscan.io/verifyContract2"
                                                   target="_blank" rel="noopener noreferrer">Etherscan</a>.
                                </Tooltip>
                            }>
                                <Button bsStyle="primary" className="btn-maxWidth"
                                        href={"/generator/session/" + this.state.sessionId + "/export/onefilesource"}
                                        rel="noopener noreferrer">
                                    Build One file source
                                </Button>
                            </OverlayTrigger>
                        </Col>
                    </Row>
                </Grid>
                <br/>
                <div style={{textAlign: "right"}}>
                    <Button
                        bsStyle="default"
                        href={"/generator/session/" + this.state.sessionId}
                        className="btnMargin">
                        <FontAwesomeIcon icon={faChevronCircleLeft}/> Contract configuration
                    </Button>
                </div>
            </div>
        );
    }
}