import React from 'react';
import "./Export.css"
import {} from 'react-bootstrap';
import Exporter from "./Exporter";
import {Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronCircleLeft} from "@fortawesome/free-solid-svg-icons";

export default class ExportBinaries extends React.Component {
	render() {
		return (
			<Exporter
				name="Binaries"
				download={"$API_URL/session/" + this.props.match.params.sessionId + "/export/binaries"}
			>
				<p>Binaries that can be deployed to any Ethereum Network.</p>
				<div style={{textAlign: "right"}}>
					<Button
						bsStyle="default"
						href={"/generator/session/" + this.props.match.params.sessionId + "/export"}
						className="btnMargin">
						<FontAwesomeIcon icon={faChevronCircleLeft}/> Export
					</Button>
				</div>
			</Exporter>
		);
	}
}