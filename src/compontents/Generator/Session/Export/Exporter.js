import React from 'react';
import "./Export.css"
import {Button, OverlayTrigger, Tooltip} from 'react-bootstrap';
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import {Toast} from "../../../Toast";

export default class Exporter extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			downloadedFile: null,
			fileName: null
		};
		this.saveDownload = this.saveDownload.bind(this);
	}

	componentDidMount() {
		axios({
			method: "GET",
			url: this.props.download,
			responseType: 'arraybuffer'
		})
			.then(response => {
				Toast.showSuccess("Generating " + this.props.name + " was successful");
				this.setState({downloadedFile: response.data, fileName: response.headers["x-file-name"]});
				console.log(response)
				this.saveDownload();
			})
			.catch(error => {
				Toast.showError("Failed to generate " + this.props.name + "!");
			});
	}

	saveDownload() {
		let file = new Blob([this.state.downloadedFile], {type: "application/octet-binary;charset=utf-8"});
		let a = document.createElement("a");
		a.href = URL.createObjectURL(file);
		a.download = this.state.fileName;
		document.body.appendChild(a);
		a.click();
	}

	render() {
		if (!this.state.downloadedFile) {
			return (
				<h1><FontAwesomeIcon icon={faSpinner} pulse/> Exporting {this.props.name}...</h1>
			);
		}
		else {
			return (
				<div>
					<h1>Export {this.props.name}</h1>
					<div style={{textAlign: "center"}}>
						<OverlayTrigger placement="bottom" overlay={
							<Tooltip id="ExporterSave">
								If the download does not start automatically click this button.
							</Tooltip>
						}>
						<Button bsStyle="primary" className="btnMargin" onClick={this.saveDownload}>Save {this.props.name}</Button>
						</OverlayTrigger>
					</div>
					<br />
					{this.props.children}
				</div>
			);
		}
	}
}