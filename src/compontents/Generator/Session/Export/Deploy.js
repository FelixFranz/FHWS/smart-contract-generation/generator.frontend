import React from 'react';
import "./Export.css"
import {Button, OverlayTrigger, Tooltip} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronCircleLeft, faBan, faSpinner} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import {Toast} from "../../../Toast";

export default class Deploy extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			sessionId: this.props.match.params.sessionId,
			injectedWeb3Supported: false,
			compiledContract: null,
			contractAddress: null
		};
		this.deploy = this.deploy.bind(this);
	}

	componentWillMount() {
		if (window.ethereum && !this.state.injectedWeb3Supported)
			this.setState({injectedWeb3Supported: true})
		else if (!window.ethereum && this.state.injectedWeb3Supported)
			this.setState({injectedWeb3Supported: false})
	}

	componentDidMount() {
		if (!this.state.compiledContract) {
			axios({
				method: "GET",
				url: "$API_URL/session/" + this.props.match.params.sessionId + "/export/binaries"
			})
				.then(response => {
					this.setState({compiledContract: response.data});
					this.deploy();
				})
				.catch(error => {
					Toast.showError("Failed to generate binaries!");
				});
		}
	}

	async deploy() {
		try {
			await window.ethereum.enable();
		} catch (e) {
			Toast.showError("Failed to connect to MetaMask! You need to accept the Connection first!");
		}
		window.web3.eth.sendTransaction(this.state.compiledContract, function (error, contractAddress) {
			if (error)
				Toast.showError("Failed to deploy contract!\n" + error.message);
			else
				this.setState({contractAddress});
		}.bind(this))
	}

	render() {
		let body;
		if (!this.state.injectedWeb3Supported)
			body = (<div>
				<h2><FontAwesomeIcon icon={faBan}/> MetaMask is not installed!</h2>
				<p>Install and configure <a href="https://metamask.io/" target="_blank"
				                            rel="noopener noreferrer">MetaMask</a> for your browser to be able to deploy
					your smart contracts!</p>
			</div>);
		else if (!this.state.compiledContract)
			body = <h2>Building Smart Contract <FontAwesomeIcon icon={faSpinner} pulse/></h2>;
		else if (!this.state.contractAddress)
			body = (
				<div>
					<p>Use MetaMask to deploy your generated Smart Contract.</p>
					<div style={{textAlign: "center"}}>
						<OverlayTrigger placement="bottom" overlay={
							<Tooltip id="ExporterDeploy">
								If the MetaMask is not automatically opened click this button.
							</Tooltip>
						}>
							<Button bsStyle="primary" className="btnMargin"
							        onClick={this.deploy}>Deploy Smart Contract using MetaMask</Button>
						</OverlayTrigger>
					</div>
				</div>
			);
		else
			body = (
				<div>
					<p>Successfully deployed your Smart Contract!</p>
					<p>Your Contract Address: <code>{this.state.contractAddress}</code></p>
				</div>
			);
		return (
			<div>
				<h1>Deploy</h1>
				{body}
				<br/>
				<div style={{textAlign: "right"}}>
					<Button
						bsStyle="default"
						href={"/generator/session/" + this.state.sessionId + "/export"}
						className="btnMargin">
						<FontAwesomeIcon icon={faChevronCircleLeft}/> Export
					</Button>
				</div>
			</div>
		);
	}
}