import React from 'react';
import "./Export.css"
import {} from 'react-bootstrap';
import Exporter from "./Exporter";
import {Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronCircleLeft} from "@fortawesome/free-solid-svg-icons";

export default class ExportOneFileSource extends React.Component {
	render() {
		return (
			<Exporter
				name="OneFileSource"
				download={"$API_URL/session/" + this.props.match.params.sessionId + "/export/onefilesource"}
			>
				<p>Source of the whole Smart Contract, inserted into one file. This file can be used for verifying your
					contract at <a href="https://etherscan.io/verifyContract2"
					               target="_blank" rel="noopener noreferrer">Etherscan</a>.</p>
				<div style={{textAlign: "right"}}>
					<Button
						bsStyle="default"
						href={"/generator/session/" + this.props.match.params.sessionId + "/export"}
						className="btnMargin">
						<FontAwesomeIcon icon={faChevronCircleLeft}/> Export
					</Button>
				</div>
			</Exporter>
		);
	}
}