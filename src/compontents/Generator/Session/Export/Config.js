import React from 'react';
import "./Export.css"
import {} from 'react-bootstrap';
import Exporter from "./Exporter";
import {Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronCircleLeft} from "@fortawesome/free-solid-svg-icons";

export default class ExportConfig extends React.Component {
	render() {
		return (
			<Exporter
				name="Config"
				download={"$API_URL/session/" + this.props.match.params.sessionId + "/export/config"}
			>
				<p>Save the config on your pc to be able to import it into the generator and modify your contract later.</p>
				<div style={{textAlign: "right"}}>
					<Button
						bsStyle="default"
						href={"/generator/session/" + this.props.match.params.sessionId + "/export"}
						className="btnMargin">
						<FontAwesomeIcon icon={faChevronCircleLeft}/> Export
					</Button>
				</div>
			</Exporter>
		);
	}
}