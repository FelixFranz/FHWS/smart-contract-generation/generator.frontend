import React from 'react';
import {ListGroup, ListGroupItem, Collapse, Button, Alert} from 'react-bootstrap';
import {faSpinner, faChevronCircleLeft, faFileExport} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";

export default class Test extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			sessionId: this.props.match.params.sessionId,
			test: null,
			testSuccess: null,
			logOpen: false
		}
	}

	componentDidMount() {
		axios({
			method: "GET",
			url: "$API_URL/session/" + this.state.sessionId + "/test"
		})
			.then(response => this.setState({test: response.data, testSuccess: true}))
			.catch(error => {
				let test = error.response.data;
				if (Object.keys(test).length === 0) test = {log: "Executing test failed!"};
				this.setState({test, testSuccess: false});
			});
	}

	render() {
		if (!this.state.test)
			return (
				<div>
					<h1><FontAwesomeIcon icon={faSpinner} pulse/> Running tests...</h1>
				</div>
			);
		else
			return (
				<div>
					<h1>Test Result</h1>
					<Alert
						bsStyle={this.state.testSuccess ? "success" : "danger"}
						style={{display: this.state.testSuccess !== null ? "auto" : "none"}}>
						<p>{this.state.testSuccess ? "Test succeded" : "Test failed"}</p>
					</Alert>
					<p style={{display: !!this.state.test.executionTime ? "auto" : "none"}}>
						<strong>Execution time:</strong> {this.state.test.executionTime}ms
					</p>
					<ListGroup>
						<ListGroupItem onClick={() => this.setState({logOpen: !this.state.logOpen})}
						               style={{background: "whitesmoke"}}>Log</ListGroupItem>
						<ListGroupItem>
							<Collapse in={this.state.logOpen}>
								<pre>{this.state.test.log}</pre>
							</Collapse>
						</ListGroupItem>
					</ListGroup>
					<div style={{textAlign: "right"}}>
						<Button
							bsStyle="default"
							href={"/generator/session/" + this.state.sessionId }
							className="btnMargin">
							<FontAwesomeIcon icon={faChevronCircleLeft}/> Contract configuration
						</Button>
						<Button
							bsStyle="primary"
							href={"/generator/session/" + this.state.sessionId + "/export"}
							disabled={!this.state.testSuccess}
							className="btnMargin">
							<FontAwesomeIcon icon={faFileExport}/> Export
						</Button>
					</div>
				</div>
			);
	}
}