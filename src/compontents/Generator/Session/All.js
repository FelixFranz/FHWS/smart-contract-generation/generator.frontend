import React from 'react';
import axios from 'axios';
import {Toast} from "../../Toast";
import {ButtonToolbar, Button, OverlayTrigger, Tooltip} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSpinner} from "@fortawesome/free-solid-svg-icons";

export default class SessionAll extends React.Component {

    constructor(props) {
        super(props);
        this.handleCreate = this.handleCreate.bind(this);
        this.handleImport = this.handleImport.bind(this);
        this.fileSelector = this.createFileSelector();
        this.state = {
            loadingCreate: false,
            loadingImport: false
        }
    }

    createFileSelector() {
        const fileSelector = document.createElement('input');
        fileSelector.setAttribute('type', 'file');
        fileSelector.setAttribute('accept', '.json');
        fileSelector.onchange = e => {
            this.setState({loadingImport: true});
            let reader = new FileReader();
            reader.onload = (p) => {
                let data = reader.result;
                data = data.split(',')[1];
                data = atob(data);
                data = JSON.parse(data);
                this.createSession(data);
            };
            reader.readAsDataURL(this.fileSelector.files[0])
        };
        fileSelector.onchange = fileSelector.onchange.bind(this);
        return fileSelector;
    }

    createSession(imports) {
        axios({
            method: "POST",
            url: "$API_URL/session",
            data: imports
        })
            .then(response => {
	            Toast.showSuccess("Session created");
                this.props.history.push("/generator/session/" + response.data.id);
            })
            .catch(response => {
                Toast.showError("Could not create Session!");
                this.setState({
                    loadingCreate: false,
                    loadingImport: false
                });
            });
    }

    handleCreate(e) {
        this.setState({loadingCreate: true});
        this.createSession();
    }

    handleImport(e) {
        this.fileSelector.click();
    }

    render() {
        return (
            <div>
                <h1>Project Management</h1>
                <p>Let's open a project by clicking one of the buttons:</p>
                <ButtonToolbar>
                    <OverlayTrigger placement="bottom" overlay={
                        <Tooltip id="SessionAllCreate">Create a <strong>clean, new project</strong>.</Tooltip>
                    }>
                        <Button
                            bsStyle="primary"
                            className="btnMargin"
                            disabled={this.state.loadingCreate || this.state.loadingImport}
                            onClick={this.handleCreate}>
                            {this.state.loadingCreate && <FontAwesomeIcon icon={faSpinner} pulse/>} Create new Project
                        </Button>
                    </OverlayTrigger>
                    <OverlayTrigger placement="bottom" overlay={
                        <Tooltip id="SessionAllImport">Import a <strong>previously exported project</strong> from your
                            PC.</Tooltip>
                    }>
                        <Button
                            bsStyle="primary"
                            className="btnMargin"
                            disabled={this.state.loadingCreate || this.state.loadingImport}
                            onClick={this.handleImport}>
                            {this.state.loadingImport && <FontAwesomeIcon icon={faSpinner} pulse/>} Import a Project
                        </Button>
                    </OverlayTrigger>
                </ButtonToolbar>
            </div>
        );
    }
}