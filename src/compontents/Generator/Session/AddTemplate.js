import React from 'react';
import {
	Button,
	ControlLabel,
	FormControl,
	FormGroup,
	Modal,
	OverlayTrigger,
	Tooltip,
	DropdownButton,
	MenuItem
} from 'react-bootstrap';
import axios from "axios";
import {Toast} from "../../Toast";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faPlus} from "@fortawesome/free-solid-svg-icons";

export default class AddTemplate extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			modalShown: false,
			availableTemplates: [],
			name: null,
			variant: null,
			constructorVariant: null,
			constructorVariable: null
		};
		this.onAddTemplate = this.onAddTemplate.bind(this);
		this.showModal = this.showModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
		this.getTemplateNames = this.getTemplateNames.bind(this);
		this.setTemplateName = this.setTemplateName.bind(this);
		this.getTemplateInformation = this.getTemplateInformation.bind(this);
		this.getTemplateVariants = this.getTemplateVariants.bind(this);
		this.setTemplateVariant = this.setTemplateVariant.bind(this);
		this.getTemplateVariantInformation = this.getTemplateVariantInformation.bind(this);
		this.getConstructorVariants = this.getConstructorVariants.bind(this);
		this.setConstructorVariant = this.setConstructorVariant.bind(this);
		this.getConstructorVariantInformation = this.getConstructorVariantInformation.bind(this);
		this.validateConstructorVariable = this.validateConstructorVariable.bind(this);
		this.isEverythingValid = this.isEverythingValid.bind(this);
		this.areConstructorVariablesValid = this.areConstructorVariablesValid.bind(this);
	}

	componentDidUpdate(){
		if (!this.state.name && this.state.availableTemplates && this.state.availableTemplates.length === 1) {
			this.setState({name: this.state.availableTemplates[0].name});
			return;
		}
		let templateInformation = this.getTemplateInformation();
		if (!this.state.variant && templateInformation && templateInformation.variants.length === 1){
			this.setState({variant: templateInformation.variants[0].name});
			return;
		}
		let templateVariantInformation = this.getTemplateVariantInformation();
		if (!this.state.constructorVariant && templateVariantInformation && templateVariantInformation.constructor.length === 1){
			let constructorVariant = templateVariantInformation.constructor[0].name;
			let parameterLength = templateVariantInformation.constructor
				.filter(v => v.name === constructorVariant)[0].parameters.length;
			this.setState({
				constructorVariant,
				constructorVariable: new Array(parameterLength)
			});
			return;
		}
	}

	onAddTemplate(e) {
		let data = {
			name: this.state.name,
			variant: this.state.variant,
			constructorVariable: this.state.constructorVariable
		};
		axios({
			method: "POST",
			url: "$API_URL/session/" + this.props.sessionId + "/template",
			data
		})
			.then(response => {
				Toast.showSuccess("Added template");
				this.props.onTemplateAdded(data);
				this.closeModal();
			})
			.catch(response => Toast.showError("Could not add template"));
	}

	showModal() {
		this.setState({
			name: null,
			variant: null,
			constructorVariant: null,
			constructorVariable: null,
			modalShown: true
		});
		axios({
			method: "GET",
			url: "$API_URL/session/" + this.props.sessionId + "/availabletemplates"
		})
			.then(response => this.setState({availableTemplates: response.data}))
			.catch(response => Toast.showError("Could not get available templates!"));
	}

	closeModal() {
		this.setState({
			modalShown: false,
		});
	}

	getTemplateNames() {
		return this.state.availableTemplates.map((a, i) =>
			<MenuItem key={i} onClick={this.setTemplateName}>{a.name}</MenuItem>)
	}

	setTemplateName(e) {
		this.setState({
			name: e.target.innerText,
			variant: null,
			constructorVariant: null
		});
	}

	getTemplateInformation() {
		if (!this.state.name) return null;
		return this.state.availableTemplates.filter(t => t.name === this.state.name)[0];
	}

	getTemplateVariants() {
		return this.getTemplateInformation().variants.map((v, i) =>
			<MenuItem key={i} onClick={this.setTemplateVariant}>{v.name}</MenuItem>)
	}

	setTemplateVariant(e) {
		this.setState({
			variant: e.target.innerText,
            constructorVariant: null
		});
	}

	getTemplateVariantInformation() {
		if (!this.state.variant) return null;
		return this.getTemplateInformation().variants.filter(v => v.name === this.state.variant)[0];
	}

	getConstructorVariants() {
		return this.getTemplateVariantInformation().constructor.map((c, i) =>
			<MenuItem key={i} onClick={this.setConstructorVariant}>{c.name}</MenuItem>)
	}

	setConstructorVariant(e) {
		let constructorVariant = e.target.innerText;
		let parameterLength = this.getTemplateVariantInformation().constructor
			.filter(v => v.name === constructorVariant)[0].parameters.length;
		this.setState({
			constructorVariant,
			constructorVariable: new Array(parameterLength)
		});
	}

	getConstructorVariantInformation() {
		if (!this.state.constructorVariant) return null;
		let constructorVariantInformation = this.getTemplateVariantInformation().constructor.filter(v => v.name === this.state.constructorVariant)[0];
		constructorVariantInformation.parameters = constructorVariantInformation.parameters.map(a => {
			switch (a.type) {
				case "string":
					a.inputType = "text";
					break;
				default:
					a.inputType = a.type;
			}
			return a;
		});
		return constructorVariantInformation;
	}

	setConstructorVariable(index, value) {
		let constructorVariable = this.state.constructorVariable;
		if (!isNaN(parseInt(value))) value = parseInt(value);
		constructorVariable[index] = value;
		this.setState({constructorVariable});
	}

	validateConstructorVariable(index, parameter) {
		let content = this.state.constructorVariable[index];
		if (!content || "" === content) return "error";
		if (parameter.type !== typeof content)
			return "error";
		if (parameter.higherequal && parameter.higherequal > content)
			return "error";
		if (parameter.higher && parameter.higher >= content)
			return "error";
		if (parameter.lower && parameter.lower <= content)
			return "error";
		if (parameter.lowerequal && parameter.lowerequal < content)
			return "error";
		return "success";
	}

	areConstructorVariablesValid() {
		let invalidVariables = this.getConstructorVariantInformation().parameters.filter((p, i) =>
			this.validateConstructorVariable(i, p) !== "success"
		);
		return invalidVariables.length === 0;
	}

	isEverythingValid() {
		return !!this.state.name && !!this.state.constructorVariant && this.areConstructorVariablesValid()
	}

	render() {
		let nameSelected;
		let variantSelected;
		let constructorSelected;
		if (!!this.state.name) {
			let templateInformation = this.getTemplateInformation();
			let url;
			if (templateInformation.url) {
				url = (
					<p>Get more information at the <a target="_blank" rel="noopener noreferrer"
					                                  href={templateInformation.url}>Ethereum
						Improvement Proposals project</a>!</p>
				);
			}
			nameSelected = (
				<div>
					<p>{templateInformation.description}</p>
					{url ? url : ""}
					<br/>
					<strong>Template variant</strong>
					<br/>
					<DropdownButton title={this.state.variant || "Select template variant"}
					                id="AddTemplate-TemplateVariant">
						{this.getTemplateVariants()}
					</DropdownButton>
				</div>
			);
		}
		if (!!this.state.variant) {
			let variantInformation = this.getTemplateVariantInformation();
			variantSelected = (
				<div>
					<p>{variantInformation.description}</p>
					<br/>
					<strong>Constructor</strong>
					<br/>
					<DropdownButton title={this.state.constructorVariant || "Select constructor variant"}
					                id="AddTemplate-TemplateVariant">
						{this.getConstructorVariants()}
					</DropdownButton>
					<br/><br/>
				</div>
			);
		}
		if (!!this.state.constructorVariant) {
			constructorSelected = this.getConstructorVariantInformation().parameters.map((c, i) => {
				return (
					<FormGroup key={i} validationState={this.validateConstructorVariable(i, c)}>
						<ControlLabel>{c.name}</ControlLabel>
						<FormControl type={c.inputType}
						             onChange={(e) => this.setConstructorVariable(i, e.target.value)}/>
						<FormControl.Feedback/>
					</FormGroup>
				);
			});
		}
		return (
			<div>
				<OverlayTrigger placement="bottom" overlay={
					<Tooltip id="SessionSpecialAddTemplate">Add new template to current Smart Contract.</Tooltip>
				}>
					<Button
						className="pull-right"
						bsStyle="primary"
						onClick={this.showModal}>
						<FontAwesomeIcon icon={faPlus}/>
					</Button>
				</OverlayTrigger>
				<Modal show={this.state.modalShown} onHide={this.closeModal}>
					<Modal.Header closeButton>
						<Modal.Title>Add a new template</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div>
							<strong>Template name</strong>
							<br/>
							<DropdownButton title={this.state.name || "Select template"} id="AddTemplate-TemplateName">
								{this.getTemplateNames()}
							</DropdownButton>
						</div>
						{nameSelected}
						{variantSelected}
						{constructorSelected}
					</Modal.Body>
					<Modal.Footer>
						<Button bsStyle="primary" onClick={this.closeModal}>Close</Button>
						<Button bsStyle="primary" onClick={this.onAddTemplate} disabled={!this.isEverythingValid()}>Add
							template</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}