import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import axios from "axios";
import {Toast} from "../../Toast";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from "@fortawesome/free-solid-svg-icons";

export default class DeleteSession extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			modalShown: false
		};
		this.onDeleteSession = this.onDeleteSession.bind(this);
		this.showModal = this.showModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	onDeleteSession(e) {
		axios({
			method: "DELETE",
			url: "$API_URL/session/" + this.props.sessionId
		})
			.then(response => {
				Toast.showSuccess("Successfully deleted project");
				this.props.history.push("/generator/session/");
			})
			.catch(response => Toast.showError("Failed to delete project!"));
	}

	showModal() {
		this.setState({
			modalShown: true
		});
	}

	closeModal() {
		this.setState({
			modalShown: false
		});
	}

	render() {
		return (
			<span>
				<Button
					bsStyle="danger" className="btnMargin" onClick={this.showModal}>
					<FontAwesomeIcon icon={faTrash}/> Delete Project
				</Button>
				<Modal show={this.state.modalShown} onHide={this.closeModal}>
					<Modal.Header closeButton>
						<Modal.Title>Delete project</Modal.Title>
					</Modal.Header>
					<Modal.Body>Do you really want to delete the template this project?</Modal.Body>
					<Modal.Footer>
						<Button bsStyle="primary" onClick={this.closeModal}>Close</Button>
						<Button bsStyle="primary" onClick={this.onDeleteSession}>Delete project</Button>
					</Modal.Footer>
				</Modal>
			</span>
		);
	}
}