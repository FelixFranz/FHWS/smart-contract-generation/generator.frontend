import React from 'react';
import {ListGroup, ListGroupItem, Button} from 'react-bootstrap';
import axios from "axios";
import {Toast} from "../../Toast";
import {faSpinner, faFastForward, faChevronCircleRight} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import MetaInfos from "./MetaInfos";
import Templates from "./Templates";
import DeleteSession from "./DeleteSession";

export default class SessionSpecial extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sessionId: this.props.match.params.sessionId,
            contract: null
        };
        this.updateMetaInformation = this.updateMetaInformation.bind(this);
        this.updateTemplates = this.updateTemplates.bind(this);
    }

    componentDidMount() {
        axios({
            method: "GET",
            url: "$API_URL/session/" + this.state.sessionId
        })
            .then(response => this.setState({contract: response.data}))
            .catch(response => Toast.showError("Could not load Session!"));
    }

    updateMetaInformation(data) {
        let contract = this.state.contract;
        Object.keys(data).forEach(d => contract[d] = data[d]);
        this.setState({contract});
    }

    updateTemplates(templates) {
        let contract = this.state.contract;
        contract.templates = templates;
        this.setState({contract});
    }

    render() {
        if (!this.state.contract)
            return <h1>Loading <FontAwesomeIcon icon={faSpinner} pulse/></h1>;
        else
            return (
                <div>
                    <h1>Your Smart Contract</h1>
                    <ListGroup>
                        <ListGroupItem>
                            <MetaInfos
                                name={this.state.contract.name}
                                compilerVersion={this.state.contract.compilerVersion}
                                sessionId={this.state.sessionId}
                                updateMetaInformation={this.updateMetaInformation}/>

                        </ListGroupItem>
                        <ListGroupItem>
                            <Templates sessionId={this.state.sessionId} templates={this.state.contract.templates}
                                       updateTemplates={this.updateTemplates}/>
                        </ListGroupItem>
                    </ListGroup>
                    <div style={{textAlign: "right"}}>
                        <DeleteSession sessionId={this.state.sessionId} history={this.props.history}/>
                        <Button
                            bsStyle="default" href={"/generator/session/" + this.state.sessionId + "/export"}
                            className={(this.state.contract.templates.length === 0 ? "disabled " : "") + "btnMargin"}>
                            <FontAwesomeIcon icon={faFastForward}/> Skip Tests
                        </Button>
                        <Button
                            bsStyle="primary" href={"/generator/session/" + this.state.sessionId + "/test"}
                            className={(this.state.contract.templates.length === 0 ? "disabled " : "") + "btnMargin"}>
                            <FontAwesomeIcon icon={faChevronCircleRight}/> Run Tests
                        </Button>
                    </div>
                </div>
            );
    }
}