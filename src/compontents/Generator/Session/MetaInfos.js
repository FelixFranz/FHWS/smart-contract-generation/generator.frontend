import React from 'react';
import {FormGroup, FormControl, ControlLabel, Button} from 'react-bootstrap';
import axios from "axios";
import {Toast} from "../../Toast";

export default class MetaInfos extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			name: this.props.name,
			compilerVersion: this.props.compilerVersion
		};
		this.handleNameChange = this.handleNameChange.bind(this);
		this.validateName = this.validateName.bind(this);
		this.handleCompilerVersionChange = this.handleCompilerVersionChange.bind(this);
		this.validateCompilerVersion = this.validateCompilerVersion.bind(this);
		this.saveMetaInfos = this.saveMetaInfos.bind(this);
	}

	handleNameChange(e) {
		this.setState({name: e.target.value});
	}

	validateName() {
		if (this.state.name.match(/^[a-z|A-Z][a-z|A-Z|0-9]{4}[a-z|A-Z|0-9]*$/))
			return "success";
		return "error";
	}

	handleCompilerVersionChange(e) {
		this.setState({compilerVersion: e.target.value});
	}

	validateCompilerVersion() {
		if (this.state.compilerVersion.match(/^[0-9]+(\.[0-9]+)*$/))
			return "success";
		return "error";
	}

	saveMetaInfos() {
		let data = {
            name: this.state.name,
            compilerVersion: this.state.compilerVersion
        };
		axios({
			method: "PUT",
			url: "$API_URL/session/" + this.props.sessionId,
			data
		})
			.then(response => {
				this.props.updateMetaInformation(data);
				Toast.showSuccess("Saved meta infos!")
            })
			.catch(response => Toast.showError("Could not save meta infos!"));
	}

	render() {
		return (
			<div>
				<FormGroup validationState={this.validateName()}>
					<ControlLabel>Contract name</ControlLabel>
					<FormControl type="text" value={this.state.name} onChange={this.handleNameChange}/>
					<FormControl.Feedback/>
				</FormGroup>
				<FormGroup validationState={this.validateCompilerVersion()}>
					<ControlLabel>Compiler Version</ControlLabel>
					<FormControl type="text" value={this.state.compilerVersion}
					             onChange={this.handleCompilerVersionChange}/>
					<FormControl.Feedback/>
				</FormGroup>
				<div style={{textAlign: "right"}}>
					<Button
						className={this.validateName() === "success" && this.validateCompilerVersion() === "success" ? "" : " disabled"}
						bsStyle="primary"
						onClick={this.saveMetaInfos}>
						Save
					</Button>
				</div>
			</div>
		);
	}
}