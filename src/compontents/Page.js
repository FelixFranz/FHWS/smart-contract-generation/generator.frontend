import React from 'react';
import NavBar from "./NavBar";
import {ToastBox} from "./Toast";
import Footer from "./Footer";

export default class Home extends React.Component {

	render() {
		return (
			<div style={{height: "100%"}}>
				<ToastBox/>
				<div style={{minHeight: "calc(100% - 40px"}}>
					<NavBar {...this.props}/>
					<div className="container">
						{this.props.children}
					</div>
				</div>
				<Footer/>
			</div>
		);
	}
}