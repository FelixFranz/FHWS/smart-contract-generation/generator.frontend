import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Page from './Page.js'
import Error from './Error.js'
import Home from './Home.js'
import Generator from './Generator/Generator.js'
import SessionAll from "./Generator/Session/All";
import SessionSpecial from "./Generator/Session/Special";
import Test from "./Generator/Session/Test";
import Export from "./Generator/Session/Export/Export";
import ExportConfig from "./Generator/Session/Export/Config";
import ExportSource from "./Generator/Session/Export/Source";
import ExportBinaries from "./Generator/Session/Export/Binaries";
import ExportOneFileSource from "./Generator/Session/Export/OneFileSource";
import Deploy from "./Generator/Session/Export/Deploy";

export default () => {
	return (
		<BrowserRouter>
			<Switch>
				<Route path='/generator/session/:sessionId/export/deploy' render={props => <Page {...props}><Deploy {...props}/></Page>} />
				<Route path='/generator/session/:sessionId/export/onefilesource' render={props => <Page {...props}><ExportOneFileSource {...props}/></Page>} />
				<Route path='/generator/session/:sessionId/export/binaries' render={props => <Page {...props}><ExportBinaries {...props}/></Page>} />
				<Route path='/generator/session/:sessionId/export/source' render={props => <Page {...props}><ExportSource {...props}/></Page>} />
				<Route path='/generator/session/:sessionId/export/config' render={props => <Page {...props}><ExportConfig {...props}/></Page>} />
				<Route path='/generator/session/:sessionId/export' render={props => <Page {...props}><Export {...props}/></Page>} />
				<Route path='/generator/session/:sessionId/test' render={props => <Page {...props}><Test {...props}/></Page>} />
				<Route path='/generator/session/:sessionId' render={props => <Page {...props}><SessionSpecial {...props}/></Page>} />
				<Route path='/generator/session' render={props => <Page {...props}><SessionAll {...props}/></Page>} />
				<Route path='/generator' render={props => <Page {...props}><Generator {...props}/></Page>} />
				<Route exact path='/' render={props => <Page {...props}><Home {...props}/></Page>} />
				<Route exact path='*' render={props => <Page {...props}><Error {...props}/></Page>} />
			</Switch>
		</BrowserRouter>
	);
};