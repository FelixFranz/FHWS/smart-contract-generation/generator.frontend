import React from 'react';
import {Jumbotron, Button, Panel} from 'react-bootstrap';

export default class Home extends React.Component {

    render() {
        return (
            <div>
                <Jumbotron>
                    <h1>Smart Contract Generator</h1>
                    <p>Welcome to the generator for Smart Contract!</p>
                    <p>After building Smart Contracts using this generator you can easily deploy them to
                        the <a href="https://ethereum.org" target="_blank"
                               rel="noopener noreferrer">Ethereum</a> Network.
                    </p>
                    <Button bsStyle="primary" href="/generator">Start building a Smart Contract</Button>
                </Jumbotron>
                <Panel>
                    <Panel.Body>
                        <h2>About this project</h2>
                        <p>
                            This Project was created
                            by <a href="https://www.felix-franz.com" target="_blank" rel="noopener noreferrer">Felix
                            Franz</a> as part of his Bachelor Thesis.
                            <br/>
                            Using pre-implemented standarts that are defined on
                            the <a href="https://eips.ethereum.org/" target="_blank" rel="noopener noreferrer">
                            Ethereum Improvement Proposals Project</a> the generator creates Smart Contracts that fits
                            to anyone's belongings.
                            <br/>
                            To keep it as simple to use as possible, the user, that wants to generate a Smart Contract,
                            only needs to select the functionality he needs.
                        </p>
                        <p>
                            Interested? - Try it out by <a href="/generator">creating a new Smart Contract</a>.
                        </p>
                    </Panel.Body>
                </Panel>
            </div>
        );
    }
}