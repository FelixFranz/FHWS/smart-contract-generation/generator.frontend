import React from 'react';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const defaultOptions = {
	position: "top-right",
	autoClose: 5000,
	hideProgressBar: false,
	closeOnClick: true,
	pauseOnHover: true,
	draggable: true
};

export const Toast = {
	checkOptions(options){
		if (options === undefined)
			options = {};
		Object.keys(defaultOptions).forEach(d => {
			if (options[d] === undefined)
				options[d] = defaultOptions[d];
		});
		return options;
	},

	showSuccess(message, options){
		options = this.checkOptions(options);
		toast.success(message, options);
	},

	showInfo(message, options){
		options = this.checkOptions(options);
		toast.info(message, options);
	},

	showWarn(message, options){
		options = this.checkOptions(options);
		toast.warn(message, options);
	},

	showError(message, options){
		options = this.checkOptions(options);
		toast.error(message, options);
	},

	showDefault(message, options){
		options = this.checkOptions(options);
		toast(message, options);
	}
};

export class ToastBox extends React.Component{
	render(){
		return <ToastContainer />
	}
}