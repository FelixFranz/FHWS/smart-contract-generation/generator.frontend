import React from 'react';
import './NavBar.css'
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faHome, faBoxes, faCog, faUserCircle, faInfoCircle} from "@fortawesome/free-solid-svg-icons";
import {faGitlab} from '@fortawesome/free-brands-svg-icons';

export default class NavBar extends React.Component {

	render() {
		return (
			<div style={{height: "60px"}}>
				<Navbar inverse collapseOnSelect fixedTop>
					<Navbar.Header>
						<Navbar.Brand>Smart Contract Generator</Navbar.Brand>
						<Navbar.Toggle/>
					</Navbar.Header>
					<Navbar.Collapse>
						<Nav pullLeft>
							<NavItem href="/" className={this.props.location.pathname.match("^/$") && "active"}>
								<FontAwesomeIcon icon={faHome}/> Home
							</NavItem>
							<NavItem href="/generator"
							         className={this.props.location.pathname.match("^/generator(/session)?/?$") && "active"}>
								<FontAwesomeIcon icon={faBoxes}/> Project
							</NavItem>
							<NavItem href={"/generator/session/" + this.props.match.params.sessionId}
							         className={this.props.location.pathname.match("^/generator/session/.+$") && "active"}
							disabled={!this.props.location.pathname.match("^/generator/session/.+$")}>
								<FontAwesomeIcon icon={faCog}/> Generator
							</NavItem>
						</Nav>
						<Nav pullRight>
							<NavItem href="https://gitlab.com/FelixFranz/FHWS/smart-contract-generation" target="_blank"><FontAwesomeIcon
								icon={faGitlab}/> Source</NavItem>
							<NavItem href="https://www.felix-franz.com" target="_blank"><FontAwesomeIcon
								icon={faUserCircle}/> Developer</NavItem>
							<NavItem href="https://fiw.fhws.de/service/rechtliches/impressum/"
							         target="_blank"><FontAwesomeIcon icon={faInfoCircle}/> About</NavItem>
						</Nav>
					</Navbar.Collapse>
				</Navbar>
			</div>
		);
	}
}