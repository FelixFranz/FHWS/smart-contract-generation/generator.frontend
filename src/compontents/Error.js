import React from 'react';
import {Redirect} from 'react-router-dom'
import {Button} from 'react-bootstrap'
import {faHome} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default class Error extends React.Component {

	render() {
		if (this.props.location.pathname !== "/error")
			return <Redirect to="/error"/>;
		else
			return (
				<div>
					<h1>Error</h1>
					<p>The page you are looking for does not exist!</p>
					<Button bsStyle="primary" href="/"><FontAwesomeIcon icon={faHome} /> Go Home</Button>
				</div>
			);
	}
}
